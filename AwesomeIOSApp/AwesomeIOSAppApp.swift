//
//  AwesomeIOSAppApp.swift
//  AwesomeIOSApp
//
//  Created by Abbas Al-Mansoori on 2023-07-25.
//

import SwiftUI

@main
struct AwesomeIOSAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
