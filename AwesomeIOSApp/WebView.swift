//
//  WebView.swift
//  AwesomeIOSApp
//
//  Created by Abbas Al-Mansoori on 2023-07-25.
//

import Foundation

import SwiftUI
import WebKit
 
struct WebView: UIViewRepresentable {
 
    var url: URL
    let contentController = ContentController()

    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }
    

    func updateUIView(_ webView: WKWebView, context: Context) {
        let request = URLRequest(url: url)
        webView.configuration.defaultWebpagePreferences.allowsContentJavaScript = true
        
        webView.configuration.userContentController.add(contentController, name: "observer")
        
        webView.evaluateJavaScript("navigator.userAgent") { [weak webView] (result, error) in
            if let webView = webView, let userAgent = result as? String {
                webView.customUserAgent = userAgent + " / ExternalP24NativeApp"
            }
        }
                
        webView.load(request)
        
    }
    
    class ContentController: NSObject, WKScriptMessageHandler {
        func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {

            if message.name == "observer"{
                print(message.body)
            }
        }
    }
}
