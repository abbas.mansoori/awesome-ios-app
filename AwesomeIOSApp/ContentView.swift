//
//  ContentView.swift
//  AwesomeIOSApp
//
//  Created by Abbas Al-Mansoori on 2023-07-25.
//

import SwiftUI

struct ContentView: View {
    @State private var showWebView = false
    
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            
            Button {
                showWebView.toggle()
            } label: {
                Text("Show Patient app")
            }
            .sheet(isPresented: $showWebView) {
                WebView(url: URL(string: "http://172.20.10.2:8888/home")!)
            }
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
